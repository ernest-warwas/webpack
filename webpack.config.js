const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {
    const isDev = env && env.development;
    return {
        // entry point
        entry: './src/javascript/index.js',

        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'bundle.js'
        },

        resolve: {
            extensions: [".js", ".jsx", ".css", ".scss"]
        },

        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: isDev
                            }
                        },
                        "postcss-loader",
                        {
                            loader: "sass-loader",
                            options: {
                                sourceMap: isDev
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif|svg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                outputPath: 'images'
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: "[name].[ext]"
                            }
                        }
                    ]
                },
            ]
        },

        plugins: [
            new MiniCssExtractPlugin({
                filename: "bundle.css"
            })
        ],

        mode: 'development'
    }
};